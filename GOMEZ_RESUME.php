<!DOCTYPE html>
<html>
<head>
	<title>RESUME</title>
	<link rel="stylesheet" href="GOMEZ_RESUME.css">
</head>
<body>
	<div class="container">
		<div class="grid header">
			
			<div class="grid content1">
				<img src="JOJO.jpg">
				<h1>Jonah Mae A. Gomez</h1>
				<hr>
			</div>
		</div>

		<div class="grid content">

			<div class="grid contenTT">
				<h2>PERSONAL INFORMATION</h2>
				<hr class="hover">

				<table>
					<tr>
						<th style="background-color:#E6E6E6">Name:</th>
                    	<td>Jonah Mae A. Gomez</td>
					</tr>
					
					<tr>
						<th>Age:</th>
						<td>19</td>
					</tr>
	
					<tr>
						<th>Address:</th>
						<td>Phase 1 blk 5 ubas  compound S.I.R Davao City</td>
					</tr>
	
					<tr>
						<th>Status:</th>
						<td>Single</td>
					</tr>
					
					<tr>
						<th>Sex:</th>
						<td>Female</td>
					</tr>
					
					<tr>
						<th>Father's Name:</th>
						<td>Jonas A. Gomez</td>
					</tr>
				   
					<tr>
						<th>Mother's Name:</th>
						<td>Melanie A. Gomez</td>
					</tr>
				</table>
			</div>
			
			<div class="grid contenTTT">
				<h2>CONTACT INFORMATION</h2>
				<hr class="hover2">

				<table>
					<tr>
                        <th>Email:</th>
                        <td>jonahgomez.13@gmail.com</td>
                    </tr>

                    <tr>
                        <th>Cellphone No.:</th>
                        <td>09172022846</td>
                    </tr>

                    <tr>
                        <th>Telephone No.:</th>
                        <td>295-1695</td>
                    </tr>
				</table>
			</div>

			<div class="grid contenTTTT">
				<h2>EDUCATIONAL BACKGROUND</h2>
				<hr class="hover3">

				<table>
					<tr>
                        <th>Primary:</th>
                        <td>Magallanes Elementary School</td>
					</tr>

					<tr>
                        <th>Secondary:</th>
                        <td>Catalunan Pequeño National High School</td>
					</tr>
					
					<tr>
                        <th>Tertiary.:</th>
                        <td>University of Southeastern Philippines</td>
                    </tr>
				</table>
			</div>

			<div  class="grid contenTTTT">
				<h2>SKILLS</h2>
				<hr class="hover4">
				<table>
					<tr>
                        <td>Web Developing</td>
					</tr>
					
					<tr>
                        <td>Multimedia Design</td>
                    </tr>
				</table>
			</div>
		</div>
	</div>

</body>
</html>